package database

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

var DB *sql.DB

func init() {
	connDB := "postgres://postgres:221102@localhost:5432/portal_berita_online?sslmode=disable"
	var err error
	DB, err = sql.Open("postgres", connDB)
	if err != nil {
		log.Fatal(err)
	}
	if err = DB.Ping(); err != nil {
		log.Fatal(err)
	}
	// migrate -database "postgres://postgres:221102@localhost:5432/portal_berita_online?sslmode=disable" -path src/migrations up
}
