CREATE TABLE IF NOT EXISTS categories (
    id UUID PRIMARY KEY,
    nama VARCHAR(250)
);


CREATE TABLE IF NOT EXISTS artikel (
    id UUID PRIMARY KEY,
    Title VARCHAR(250) NOT NULL,
    kontent TEXT,
    image_url VARCHAR(250),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    category_id UUID REFERENCES categories(id)
);