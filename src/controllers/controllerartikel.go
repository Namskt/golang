package controllers

import (
	"encoding/json"
	"net/http"
	"portal-berita-online/src/database"
	"portal-berita-online/src/models"
	"time"

	"github.com/google/uuid"
)

func AddCategory(w http.ResponseWriter, r *http.Request) {
	var addcategory models.Category
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&addcategory); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	defer r.Body.Close()

	NewUUId := uuid.New()
	addcategory.ID = NewUUId.String()

	if addcategory.Nama == "" {
		http.Error(w, "Kategori tidak boleh kosong", http.StatusBadRequest)
		return
	}

	_, err := database.DB.Exec("INSERT INTO categories (id, nama) VALUES ($1, $2)", addcategory.ID, addcategory.Nama)
	if err != nil {
		http.Error(w, "Gagal menambahkan kategori", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(addcategory)

}
func GetCategory(w http.ResponseWriter, r *http.Request) {
	// Membuat query untuk mengambil daftar kategori
	query := "SELECT id, name FROM categories"
	rows, err := database.DB.Query(query)
	if err != nil {
		http.Error(w, "Gagal menjalankan query", http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var categories []models.Category // Ganti dengan tipe data yang sesuai

	// Loop melalui hasil query dan mengisi slice categories
	for rows.Next() {
		var category models.Category
		if err := rows.Scan(&category.ID, &category.Nama); err != nil {
			http.Error(w, "Gagal membaca kategori", http.StatusInternalServerError)
			return
		}
		categories = append(categories, category)
	}

	// Mengubah hasil ke format JSON dan mengirimkannya sebagai respons
	jsonResponse, err := json.Marshal(categories)
	if err != nil {
		http.Error(w, "Gagal mengonversi data ke JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonResponse)
}
func AddArtikel(w http.ResponseWriter, r *http.Request) {
	var addartikel models.Artikel
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&addartikel); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	defer r.Body.Close()

	NewUUID := uuid.New()
	addartikel.ID = NewUUID.String()

	qery := "INSERT INTO artikel (id, title, kontent, image_url, created_at, updated_at, category_id) VALUES ($1, $2, $3, $4, $5, $6, $7)"
	_, err := database.DB.Exec(qery, addartikel.ID, addartikel.Title, addartikel.Kontent, addartikel.Image_Url, time.Now(), time.Now(), addartikel.Category_id.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	rows, err := database.DB.Query(`SELECT id, nama FROM categories`)
	if err != nil {
		http.Error(w, "Gagal mengambil daftar kategori", http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var categories []models.Category
	for rows.Next() {
		var category models.Category
		if err := rows.Scan(&category.ID, &category.Nama); err != nil {
			http.Error(w, "Gagal membaca kategori", http.StatusInternalServerError)
			return
		}
		_ = append(categories, category)
	}

}
func GetAllArtikel(w http.ResponseWriter, r *http.Request) {

}
func GetById(w http.ResponseWriter, r *http.Request) {

}
func UpdateArtikel(w http.ResponseWriter, r *http.Request) {

}
func DeleteArtikel(w http.ResponseWriter, r *http.Request) {

}
