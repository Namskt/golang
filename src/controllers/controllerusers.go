package controllers

import (
	"encoding/json"
	"net/http"
	"portal-berita-online/src/database"
	"portal-berita-online/src/jwt"
	"portal-berita-online/src/models"
	"strings"

	_ "github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

func Register(w http.ResponseWriter, r *http.Request) {
	var register models.Users
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&register); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	NewUUID := uuid.New()
	register.ID = NewUUID.String()

	if register.Nama_Lengkap == "" || register.Username == "" || register.Email == "" || register.Pass == "" {
		http.Error(w, "Data belum lengkap", http.StatusBadRequest)
		return
	}

	var existingUser models.Users
	err := database.DB.QueryRow("SELECT id FROM users WHERE username = $1 OR email = $2", register.Username, register.Email).Scan(&existingUser.ID)
	if err == nil {
		http.Error(w, "Username atau Password sudah digunakan", http.StatusConflict)
		return
	}

	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(register.Pass), bcrypt.DefaultCost)
	register.Pass = string(hashedPassword)

	query := "INSERT INTO users (id, nama_lengkap, username, email, pass) VALUES ($1, $2, $3, $4, $5)"
	_, err = database.DB.Exec(query, register.ID, register.Nama_Lengkap, register.Username, register.Email, register.Pass)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Generate a JWT token upon successful registration
	token, err := jwt.GenerateToken(register)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response := map[string]string{"success": "true", "message": "Registration successful", "token": token}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}
func Login(w http.ResponseWriter, r *http.Request) {
	var login models.Users
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&login); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	var storedPassword string
	err := database.DB.QueryRow("SELECT pass FROM users WHERE username = $1", login.Username).Scan(&storedPassword)
	if err != nil {
		http.Error(w, "Katasandi Tidak Valid", http.StatusUnauthorized)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(storedPassword), []byte(login.Pass))
	if err != nil {
		http.Error(w, "Kata sandi tidak validf", http.StatusUnauthorized)
		return
	}
	// Generate a JWT token upon successful login
	token, err := jwt.GenerateToken(login)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	response := map[string]string{"success": "true", "message": "Login berhasil", "token": token}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}
func LogOut(w http.ResponseWriter, r *http.Request) {
	var logout models.Users
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&logout); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	// Verifikasi username dan token
	tokenString := r.Header.Get("Authorization")
	if tokenString == "" {
		http.Error(w, "Token tidak valid", http.StatusUnauthorized)
		return
	}
	tokenString = strings.TrimPrefix(tokenString, "Bearer ")

	token, err := jwt.ValidateToken(tokenString)
	if err != nil {
		http.Error(w, "Token tidak valid", http.StatusUnauthorized)
		return
	}

	claims, ok := token.Claims.(*jwt.CustomClaims)
	if !ok {
		http.Error(w, "Token tidak valid", http.StatusUnauthorized)
		return
	}

	// Periksa apakah ID pengguna dalam token cocok dengan ID pengguna yang melakukan logout
	if claims.UserID != logout.ID {
		http.Error(w, "Tidak diotorisasi", http.StatusUnauthorized)
		return
	}

	// Di sini Anda dapat menghapus token dari pemetaan UserTokens atau melakukan tindakan logout lainnya

	response := map[string]string{"message": "Logout berhasil"}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}
