package routers

import (
	"portal-berita-online/src/controllers"

	"github.com/go-chi/chi/v5"
)

func RouterArtikel(r chi.Router) {
	r.Route("/artikel", func(r chi.Router) {
		r.Post("/add-category", controllers.AddCategory)
		r.Post("/add-artikel", controllers.AddArtikel)
		r.Get("/get-category", controllers.GetCategory)
	})
}
