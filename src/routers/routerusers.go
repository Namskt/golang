package routers

import (
	"portal-berita-online/src/controllers"

	"github.com/go-chi/chi/v5"
)

func RouterUsers(r chi.Router) {
	r.Route("/users", func(r chi.Router) {
		r.Post("/register", controllers.Register)
		r.Post("/login", controllers.Login)
		r.Post("/logout", controllers.LogOut)
	})
}
