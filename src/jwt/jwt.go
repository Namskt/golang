package jwt

import (
	"portal-berita-online/src/models"

	"github.com/golang-jwt/jwt/v5"
)

var jwtSecret = []byte("dejkfnmdsf732re123jke16e9q6wdw8d9237148")

func GenerateToken(user models.Users) (string, error) {
	var claims models.CustomClaims

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims.UserID)
	tokenString, err := token.SignedString(jwtSecret)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func ValidateToken(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})

	if err != nil {
		return nil, err
	}

	return token, nil
}
