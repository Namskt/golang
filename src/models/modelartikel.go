package models

import (
	"time"
)

type Category struct {
	ID   string
	Nama string
}

type Artikel struct {
	ID          string
	Title       string
	Kontent     string
	Image_Url   string
	Created_at  time.Time
	Updated_at  time.Time
	Category_id Category
}
