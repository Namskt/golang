package models

type Users struct {
	ID           string
	Nama_Lengkap string
	Username     string
	Email        string
	Pass         string
}
type CustomClaims struct {
	UserID string `json:"user_id"`
}
